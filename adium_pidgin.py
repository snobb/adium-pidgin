#!/usr/bin/env python
#
# xmldoc.py
# Author: Alex Kozadaev (2014)
#

import xml.dom.minidom as xmldom

class Event(object):
    def __init__(self, **argv):
        self.__dict__.update(argv)

class Message(object):
    def __init__(self, **argv):
        self.__dict__.update(argv)

def read(filename):
    """ Parsing the xml document and return the xml dom object"""
    try:
        doc = xmldom.parse(filename);
    except IOError as e:
        print("error: {}".format(e.message))
        exit
    return doc

def handle_events(doc):
    """handle event nodes"""
    msg_nodes = doc.getElementsByTagName("event")
    return [handle_event(m) for m in msg_nodes]

def handle_event(event):
    """handle a single event node"""
    args = event.attributes.items()
    return Event(**dict(args))

def handle_messages(doc):
    """handle message nodes"""
    msg_nodes = doc.getElementsByTagName("message")
    return [handle_message(m) for m in msg_nodes]

def handle_message(msg):
    """handle a single message node"""
    args = msg.attributes.items()
    text = get_text(msg)
    print text
    if text:
        args.append((u"text", text))
    return Message(**dict(args))

def get_text(msg):
    """get text function (included a recursive version to clean up the embedded
    html tags"""
    def get_text_recursive(msg, rc):
        if msg.hasChildNodes:
            for child in msg.childNodes:
                if child.nodeType != child.TEXT_NODE:
                    get_text_recursive(child, rc)
                else:
                    rc.append(child.data)
        return rc
    return " ".join(get_text_recursive(msg, []))

if __name__ == "__main__":
    doc = read("sample.xml")
    events = handle_events(doc)
    for e in events:
        print e.__dict__

    msgs = handle_messages(doc)
    for m in msgs:
        print m.__dict__

# vim: ts=4 sts=4 sw=4 tw=80 ai smarttab et fo=rtcq list
